﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVP_Blog.Models;

namespace MVP_Blog.Controllers
{
    public class BlogController : Controller
    {

        public ActionResult Index()
        {
            using (var manager = new BlogManager())
            {
                var posts = manager.GetLatesPosts(10);
                return View("Index", posts);
            }          
        }

        public ActionResult Archive(int year, int month)
        {
            using (var manager = new BlogManager())
            {
                var posts = manager.GetPostsByDate(year, month);
                return View(posts);
            }
        }

        public ActionResult ViewPost(string code) 
        {
            using (var manager = new BlogManager())
            {
                var post = manager.GetPost(code);
                if (post == null)
                    return HttpNotFound();
                else
                    return View(post);
            }
        }

    }
}
